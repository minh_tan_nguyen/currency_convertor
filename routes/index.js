const axios = require('axios');
const { parseString } = require('xml2js');


exports.getDataFromXml = async (req, res) => {
  try {
    const xmlUrl = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
    
    const response = await axios.get(xmlUrl);
    const xmlData = response.data;

    parseString(xmlData, (err, result) => {
      if (err) {
        console.error(err);
        res.status(500).send('Internal Server Error');
        return;
      }

      const jsonData = JSON.stringify(result);
      res.status(200).send(jsonData);
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};

exports.postDataToJSON = async (req, res) => {
  try {
    // Fetch XML data from the provided URL
    const xmlResponse = await axios.get('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml');

    // Parse XML to JSON
    parseString(xmlResponse.data, (err, result) => {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: 'Error parsing XML' });
      }

      // Extract relevant data
      const objectRates = result && result["gesmes:Envelope"] && result["gesmes:Envelope"].Cube && result["gesmes:Envelope"].Cube[0].Cube && result["gesmes:Envelope"].Cube[0].Cube[0].Cube;

      // Get user input parameters
      const { amount, currency, currencyConvert } = req.body;

      let exchangeRate = objectRates.find(rate => rate.$.currency === currency),
        currencyConvertRate = objectRates.find(rate => rate.$.currency === currencyConvert);

      currency === "EUR" ? exchangeRate = {$:{rate:1}} : "";
      currencyConvert === "EUR" ? currencyConvertRate = {$:{rate:1}} : "";

      if (!exchangeRate || !currencyConvertRate) {
        return res.status(400).json({ error: 'Invalid currency code' });
      }

      const rate = (currencyConvertRate.$.rate) / (exchangeRate.$.rate);
      // Convert the amount to currency need Convert
      const convertedAmount = (amount * rate).toFixed(2);

      // Send the converted amount as a response
      res.json({ convertedAmount, currency: currencyConvert });
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};