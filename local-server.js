const express = require('express');
const { getDataFromXml, postDataToJSON } = require('./routes/index');

const app = express();
app.use(express.json());
const port = 3000;

app.get('/', getDataFromXml)
app.post('/', postDataToJSON);

app.listen(port, () => {
  console.log(`Local server is running at http://localhost:${port}`);
});